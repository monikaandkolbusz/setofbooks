#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;


struct Book {
    int index;
    string author_name;
    string author_surname;
    string title;
    
    struct X {
        int previous_index;
        int next_index;
    } series;
};

void getInfo (Book &myBook) {                         //wypisanie informacji o danej ksiazce na podstawie nr indeksu ksiazki
    cout << endl << "Nr indeksu: " << myBook.index+1 << endl <<"Autor:" << myBook.author_name << " " << myBook.author_surname ;
    cout << endl << "Tytul: " << myBook.title << endl;
    if(myBook.series.previous_index !=0 || myBook.series.next_index !=0 ) {
        cout << "Ksiazka nalezy do serii ksiazek." << endl;
    }
}

void getCollection (Book collection[], int n) {        //wypisanie calego zbioru ksiazke - autor + tytul
    for (int i=0; i<n; i++) {
        cout << collection[i].author_name[0] << ". " << collection[i].author_surname << "   " << "\"" << collection[i].title << "\"" << endl;
    }
}

void searchBook_title (Book collection[], int n, string title){     //wyszukiwanie ksiazki po tytule
    bool k=true;
    for(int i=0; i<n; i++) {
        if(collection[i].title == title) {              //nie rozroznia duzych liter :( stricmp
            getInfo(collection[i]);
            k=false;
            break;
        }
    }
    if(k)
        cout << "Nie ma ksiazki o takim tytule." << endl;

}

void searchBook_author (Book collection[], int n, string author) {      //wyszukiwanie ksiazki po autorze
    bool k=true;
    for(int i=0; i<n; i++) {
        if(collection[i].author_surname == author) {              //nie rozroznia duzych liter :(
            getInfo(collection[i]);
            k=false;
        }
    }
    if(k)
        cout << "Nie ma ksiazki tego autora." << endl;
}

void getSeries (Book collection[], int n, string title) {       //wypisywanie serii
    Book myBook;
    for(int i=0; i<n; i++) {
        if(collection[i].title == title) {
            myBook = collection[i];
            break;
        }
    }
    if(myBook.series.next_index == 0 && myBook.series.previous_index == 0) {
        cout << "Ksiazka nie nalezy do serii." << endl;
    }
    else {                                                  //wyszukiwanie pierwszej ksiazki z serii
        Book first, last;
        for(int j=myBook.index; j>=0; j--) {
            if (collection[j].series.previous_index == 0 && collection[j].series.next_index != 0) {
                first = collection[j];
                break;
            }
        }
        for(int j=myBook.index; j<n; j++) {                 //wyszukiwanie ostatniej ksiazki z serii
            if (collection[j].series.previous_index != 0 && collection[j].series.next_index == 0) {
                last = collection[j];
                break;
            }
        }

        for(int j=first.index; j<=last.index; j++) {        //wypisywanie serii
            getInfo(collection[j]);
        }

    }
}



int main () {
    ifstream stream;
    stream.open("spisKsiazek.txt");
    if (stream.good() != true) {                                    //sprawdzenie, czy plik zostal poprawnie otworzony
        cout << "Plik nie zostal poprawnie otworzony." ;
        return 0;
    }
    int numberOfBooks;
    stream >> numberOfBooks;                                        //wczytanie ilosci ksiazek
    Book * collection = new Book[numberOfBooks];                    //utworzenie tablicy struktur (ilosc struktur=ilosc ksiazek)
    const short sze = 50;
    char ciag_znak[sze];
    for(int j=0; j<numberOfBooks; j++) {                          //wyplenienie struktur informacjami
        stream.getline(ciag_znak, sze, '\t');
        collection[j].index=atoi(ciag_znak);
        stream.getline(ciag_znak, sze, '\t');
        collection[j].author_name=string(ciag_znak);
        stream.getline(ciag_znak, sze, '\t');
        collection[j].author_surname=string(ciag_znak);
        stream.getline(ciag_znak, sze, '\t');
        collection[j].title=string(ciag_znak);
        stream.getline(ciag_znak, sze, '\t');
        collection[j].series.previous_index=atoi(ciag_znak);
        stream.getline(ciag_znak, sze, '\n');
        collection[j].series.next_index=atoi(ciag_znak);

    }


     //testowanie fun wypisujacej inf o ksiazce
    cout << "Wybierz numer indeksu ksiazki o ktorej chcesz uzyskac wiecej informacji: ";
    int i;
    cin >> i;
    if(0<i && i<16) {
        getInfo(collection[i-1]);
    }
    else {
        cout << "Zly numer indeksu" << endl;
    }
    cout << endl << "Zbior ksiazek:" << endl;


    //testowanie fun wypisujacej zbior ksiazek
    getCollection(collection, numberOfBooks);


    //testowanie fun wyszukujacej dana ksiazke
    cout << "Jesli chcesz wyszukac ksiazke po autorze nacisnij 1, jeseli po tytule nacisnij 2.";
    int k;
    string word;
    cin >> k;
    switch (k) {
    case 1:
        cout << "Podaj nazwisko autora" << endl;
        cin.sync();
        getline(cin, word);
        searchBook_author(collection, numberOfBooks, word);
        break;
    case 2:
        cout << "Podaj tytu� ksi��ki" << endl;
        cin.sync();
        getline(cin, word);
        searchBook_title(collection, numberOfBooks, word);
        break;
    default:
        cout << "Error" << endl;
        break;
    }


    //testowanie fun wypisujacej serie
    cout << "Podaj tytul ksiazki, w celu sprawdzenia, czy nalezy do serii." << endl;
    cin.sync();
    string temp;
    getline(cin, temp);
    getSeries(collection, numberOfBooks, temp);


    return 0;
}
